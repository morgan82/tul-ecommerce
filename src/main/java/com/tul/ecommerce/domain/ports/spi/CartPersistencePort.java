package com.tul.ecommerce.domain.ports.spi;

import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.domain.model.ItemDomain;

import java.util.Set;

public interface CartPersistencePort {

    CartDomain findCartById(String cartId);

    CartDomain create(CartDomain cart);

    CartDomain update(CartDomain cart);

    Set<ItemDomain> listItems(String cartId);

    CartDomain addItem(String cartId, ItemDomain itemDomain);

    CartDomain removeItem(String cartId, String itemId);

    CartDomain updateItem(String cartId, String itemId, ItemDomain itemDomain);
}
