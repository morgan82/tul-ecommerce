package com.tul.ecommerce.domain.ports.api;

import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.domain.model.ItemDomain;

import java.util.Set;

public interface CartService {
    Set<ItemDomain> listItems(String cartId);

    CartDomain createCart();

    CartDomain addItem(String cartId, ItemDomain itemDomain);

    CartDomain removeItem(String cartId, String itemId);

    CartDomain getCart(String cartId);

    CartDomain updateItem(String cartId, String itemId, ItemDomain itemDomain);

    CartDomain checkout(String cartId);
}
