package com.tul.ecommerce.domain.ports.api;

import com.tul.ecommerce.domain.model.ProductTypeDomain;

import java.util.List;

public interface ProductTypeService {
    List<ProductTypeDomain> listProductTypes();
}
