package com.tul.ecommerce.domain.ports.api;

import com.tul.ecommerce.domain.model.ProductDomain;

import java.util.List;

public interface ProductService {
    List<ProductDomain> listProducts();

    ProductDomain createProduct(ProductDomain p);

    ProductDomain updateProduct(String sku,ProductDomain p);

    void deleteProduct(String sku);
}
