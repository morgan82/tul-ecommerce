package com.tul.ecommerce.domain.ports.spi;

import com.tul.ecommerce.domain.model.ProductDomain;

import java.util.List;
import java.util.Optional;

public interface ProductPersistencePort {

    Optional<ProductDomain> findProductBySku(String sku);

    List<ProductDomain> list();

    ProductDomain save(ProductDomain p);

    ProductDomain update(String sku, ProductDomain p);

    void delete(String sku);
}
