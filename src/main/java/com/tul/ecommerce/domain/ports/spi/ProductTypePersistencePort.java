package com.tul.ecommerce.domain.ports.spi;

import com.tul.ecommerce.domain.model.ProductTypeDomain;

import java.util.List;

public interface ProductTypePersistencePort {
    List<ProductTypeDomain> listProductTypes();
}
