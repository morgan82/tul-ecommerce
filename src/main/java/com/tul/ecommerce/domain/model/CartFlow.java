package com.tul.ecommerce.domain.model;

public enum CartFlow {
    STARTED,
    REMOVED,
    COMPLETED
}
