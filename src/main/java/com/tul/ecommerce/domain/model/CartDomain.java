package com.tul.ecommerce.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class CartDomain {
    private String cartId;
    private Set<ItemDomain> itemDomains;
    private CartFlow cartFlow = CartFlow.STARTED;
    private Double total;

    public CartDomain(CartFlow cartFlow) {
        this.cartFlow = cartFlow;
    }

}
