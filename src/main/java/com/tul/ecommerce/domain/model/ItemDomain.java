package com.tul.ecommerce.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemDomain {
    private String itemId;
    private ProductDomain productDomain;
    private Long amount;
    private Double totalItem;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDomain itemDomain = (ItemDomain) o;
        return Objects.equals(productDomain, itemDomain.productDomain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productDomain);
    }
}
