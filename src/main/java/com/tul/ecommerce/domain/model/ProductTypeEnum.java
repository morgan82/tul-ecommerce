package com.tul.ecommerce.domain.model;

public enum ProductTypeEnum {
    SIMPLE,
    DISCOUNT
}
