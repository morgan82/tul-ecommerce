package com.tul.ecommerce.domain.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;


@Setter
@Getter
public class ProductDomain {
    private String sku;
    private String name;
    private String detail;
    private Double price;
    private ProductTypeDomain type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDomain productDomain = (ProductDomain) o;
        return sku.equals(productDomain.sku);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku);
    }
}
