package com.tul.ecommerce.domain.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductTypeDomain {
    private String productTypeIdentifier;
    private String detail;
    private ProductTypeEnum type;
    private Double discount;
}
