package com.tul.ecommerce.domain.service;

import com.tul.ecommerce.application.configuration.error.BadRequestException;
import com.tul.ecommerce.domain.model.ProductDomain;
import com.tul.ecommerce.domain.ports.api.ProductService;
import com.tul.ecommerce.domain.ports.spi.ProductPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductPersistencePort productPersistence;

    public ProductServiceImp(ProductPersistencePort productPersistence) {
        this.productPersistence = productPersistence;
    }

    @Override
    public List<ProductDomain> listProducts() {
        return productPersistence.list();
    }

    @Override
    public ProductDomain createProduct(ProductDomain p) {
        existenceSkuValidate(p);
        return productPersistence.save(p);
    }

    @Override
    public ProductDomain updateProduct(String sku, ProductDomain p) {
        existenceSkuValidate(p, sku);
        return productPersistence.update(sku, p);
    }


    @Override
    public void deleteProduct(String sku) {
        productPersistence.delete(sku);
    }

    //private methods
    private void existenceSkuValidate(ProductDomain p, String sku) {
        Optional<ProductDomain> productBySku = this.productPersistence.findProductBySku(p.getSku());
        if (productBySku.isPresent() && !productBySku.get().getSku().equals(sku)) {
            throw new BadRequestException("Ya existe otro producto con ese sku");
        }
    }

    private void existenceSkuValidate(ProductDomain p) {
        boolean present = this.productPersistence.findProductBySku(p.getSku()).isPresent();
        if (present) {
            throw new BadRequestException("Ya existe otro producto con ese sku");
        }
    }
}
