package com.tul.ecommerce.domain.service;

import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.domain.ports.api.ProductTypeService;
import com.tul.ecommerce.domain.ports.spi.ProductTypePersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductTypeServiceImp implements ProductTypeService {
    private final ProductTypePersistencePort productTypePersistencePort;

    public ProductTypeServiceImp(ProductTypePersistencePort productTypePersistencePort) {
        this.productTypePersistencePort = productTypePersistencePort;
    }

    @Override
    public List<ProductTypeDomain> listProductTypes() {
        return this.productTypePersistencePort.listProductTypes();
    }
}
