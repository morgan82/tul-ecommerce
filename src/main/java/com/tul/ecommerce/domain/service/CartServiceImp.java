package com.tul.ecommerce.domain.service;

import com.tul.ecommerce.application.configuration.error.BadRequestException;
import com.tul.ecommerce.application.configuration.error.NotFoundException;
import com.tul.ecommerce.application.configuration.error.ResourceExistException;
import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.domain.model.CartFlow;
import com.tul.ecommerce.domain.model.ItemDomain;
import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.domain.model.ProductTypeEnum;
import com.tul.ecommerce.domain.ports.api.CartService;
import com.tul.ecommerce.domain.ports.spi.CartPersistencePort;
import com.tul.ecommerce.domain.ports.spi.ProductPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CartServiceImp implements CartService {
    private final CartPersistencePort cartPersistencePort;
    private final ProductPersistencePort productPersistencePort;

    public CartServiceImp(CartPersistencePort cartPersistencePort, ProductPersistencePort productPersistencePort) {
        this.cartPersistencePort = cartPersistencePort;
        this.productPersistencePort = productPersistencePort;
    }

    @Override
    public CartDomain createCart() {
        return this.cartPersistencePort.create(new CartDomain(CartFlow.STARTED));
    }

    @Override
    public CartDomain getCart(String cartId) {
        return this.cartPersistencePort.findCartById(cartId);
    }

    @Override
    public Set<ItemDomain> listItems(String cartId) {
        return this.cartPersistencePort.listItems(cartId);
    }

    @Override
    public CartDomain addItem(String cartId, ItemDomain itemDomain) {
        CartDomain cart = this.cartPersistencePort.findCartById(cartId);
        cartCompletedValidate(cart);
        if (cart.getItemDomains().contains(itemDomain)) {
            throw new ResourceExistException("Item ya existente en el carrito");
        }
        return this.cartPersistencePort.addItem(cartId, itemDomain);
    }


    @Override
    public CartDomain removeItem(String cartId, String itemId) {
        CartDomain cart = this.cartPersistencePort.findCartById(cartId);
        cartCompletedValidate(cart);
        if (cart.getItemDomains().stream().noneMatch(i -> i.getItemId().equals(itemId))) {
            throw new NotFoundException("Item a eliminar no encontrado en el carrito");
        }
        return this.cartPersistencePort.removeItem(cartId, itemId);
    }

    @Override
    public CartDomain updateItem(String cartId, String itemId, ItemDomain itemDomain) {
        CartDomain cart = this.cartPersistencePort.findCartById(cartId);
        cartCompletedValidate(cart);
        Optional<ItemDomain> itemToUpdate = cart.getItemDomains().stream().filter(itemDomain1 -> itemDomain1.getItemId().equals(itemId)).findFirst();
        if (itemToUpdate.isEmpty()) {
            throw new NotFoundException("Item a actualizar no encontrado en el carrito");
        }
        if (isProductDuplicate(cart, itemDomain, itemToUpdate)) {
            throw new BadRequestException("No se puede modificar un item con un producto que ya existe en otro item dentro del carrito");
        }
        if (isProductChange(itemDomain, itemToUpdate) && isProductNotExist(itemDomain.getProductDomain().getSku())) {
            throw new NotFoundException("No existe el producto en el item a acutalizar");
        }
        return this.cartPersistencePort.updateItem(cartId, itemId, itemDomain);
    }

    @Override
    public CartDomain checkout(String cartId) {
        CartDomain cart = this.cartPersistencePort.findCartById(cartId);
        cartCompletedValidate(cart);
        cart.setTotal(calculateTotals(cart));
        cart.setCartFlow(CartFlow.COMPLETED);
        this.cartPersistencePort.update(cart);

        return cart;
    }

    //private methods
    private boolean isProductNotExist(String sku) {
        return this.productPersistencePort.findProductBySku(sku).isEmpty();
    }

    private boolean isProductChange(ItemDomain itemDomain, Optional<ItemDomain> itemToUpdate) {
        return !itemToUpdate.get().getProductDomain().equals(itemDomain.getProductDomain());
    }

    private boolean isProductDuplicate(CartDomain cartById, ItemDomain itemDomain, Optional<ItemDomain> itemToUpdate) {
        List<ItemDomain> itemsSkuCon = cartById.getItemDomains().stream()
                .filter(i -> i.getProductDomain().getSku().equals(itemDomain.getProductDomain().getSku()))
                .collect(Collectors.toList());

        return itemsSkuCon.size() > 0 && !itemsSkuCon.contains(itemToUpdate.get());

    }

    private Double calculateTotals(CartDomain cart) {
        return cart.getItemDomains().stream().peek(i -> {
            ProductTypeDomain type = i.getProductDomain().getType();
            if (type.getType().equals(ProductTypeEnum.DISCOUNT)) {
                i.setTotalItem(i.getAmount() * i.getProductDomain().getPrice() * (1 - type.getDiscount()));
            } else {
                i.setTotalItem(i.getAmount() * i.getProductDomain().getPrice());
            }
        }).map(ItemDomain::getTotalItem).reduce(0D, Double::sum);
    }

    private void cartCompletedValidate(CartDomain cart) {
        if (CartFlow.COMPLETED == cart.getCartFlow()) {
            throw new BadRequestException("Carrito cerrado");
        }
    }
}
