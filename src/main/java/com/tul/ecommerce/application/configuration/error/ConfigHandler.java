package com.tul.ecommerce.application.configuration.error;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;


@ControllerAdvice
class ConfigHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ErrorHTTPDTO> handle(NotFoundException e, HttpServletRequest request) {

        HttpStatus notFound = HttpStatus.NOT_FOUND;
        ErrorHTTPDTO errorHTTP = new ErrorHTTPDTO();
        errorHTTP.setCodigo(notFound.value());
        errorHTTP.setEstado(notFound.getReasonPhrase());
        errorHTTP.setDetalle(e.getMessage());

        return ResponseEntity.status(notFound).body(errorHTTP);
    }

    @ExceptionHandler({ResourceExistException.class, BadRequestException.class})
    public ResponseEntity<ErrorHTTPDTO> handle(Exception e, HttpServletRequest request) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ErrorHTTPDTO errorHTTP = new ErrorHTTPDTO();
        errorHTTP.setCodigo(badRequest.value());
        errorHTTP.setEstado(badRequest.getReasonPhrase());
        errorHTTP.setDetalle(e.getMessage());

        return ResponseEntity.status(badRequest).body(errorHTTP);

    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorHTTPDTO errorHTTP = new ErrorHTTPDTO();
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        errorHTTP.setCodigo(badRequest.value());
        errorHTTP.setEstado(badRequest.getReasonPhrase());
        errorHTTP.setDetalle(ex.getBindingResult().getAllErrors().stream().map((e) -> ((FieldError) e).getField() + " " + e.getDefaultMessage()).collect(Collectors.joining(", ")));

        return ResponseEntity.status(badRequest).body(errorHTTP);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorHTTPDTO errorHTTP = new ErrorHTTPDTO();
        errorHTTP.setCodigo(status.value());
        errorHTTP.setEstado(status.getReasonPhrase());
        errorHTTP.setDetalle(ex.getMessage());

        return ResponseEntity.status(status).body(errorHTTP);

    }
}
