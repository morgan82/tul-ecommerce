package com.tul.ecommerce.application.configuration.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    public static final String MICROSERVICES_PRODUCT_TAG = "api-tul-ecommerce-product";
    public static final String MICROSERVICES_CART_TAG = "api-tul-ecommerce-cart";
    public static final String MICROSERVICES_PRODUCT_TYPE_TAG = "api-tul-ecommerce-product-type";

    @Autowired
    private BuildProperties buildProperties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tul.ecommerce.infraestructure.inbound.api"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());

    }

    /**
     * Returns custom information about the API.
     *
     * @return an {@link ApiInfo}
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Ecommerce TUL API")
                .description("API to buy construction products")
                .version(buildProperties.getVersion())
                .build();
    }

}
