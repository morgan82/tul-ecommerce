package com.tul.ecommerce.application.configuration.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ErrorHTTPDTO {
    private String estado;
    private int codigo;
    private String detalle;
}
