package com.tul.ecommerce.infraestructure.outbound.repository;

import com.tul.ecommerce.infraestructure.outbound.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemEntityRepository extends JpaRepository<Item, String> {
}
