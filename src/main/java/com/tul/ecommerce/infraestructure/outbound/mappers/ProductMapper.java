package com.tul.ecommerce.infraestructure.outbound.mappers;

import com.tul.ecommerce.domain.model.ProductDomain;
import com.tul.ecommerce.infraestructure.outbound.entity.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = ProductTypeMapper.class)
public interface ProductMapper {

    Product toEntity(ProductDomain productDomain);

    ProductDomain toDomain(Product product);
}
