package com.tul.ecommerce.infraestructure.outbound.adapters;

import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.domain.ports.spi.ProductTypePersistencePort;
import com.tul.ecommerce.infraestructure.outbound.mappers.ProductTypeMapper;
import com.tul.ecommerce.infraestructure.outbound.repository.ProductTypeEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductTypeJpaAdapter implements ProductTypePersistencePort {
    private final ProductTypeEntityRepository productTypeEntityRepository;
    private final ProductTypeMapper productTypeMapper;

    public ProductTypeJpaAdapter(ProductTypeEntityRepository productTypeEntityRepository, ProductTypeMapper productTypeMapper) {
        this.productTypeEntityRepository = productTypeEntityRepository;
        this.productTypeMapper = productTypeMapper;
    }

    @Override
    public List<ProductTypeDomain> listProductTypes() {
        return this.productTypeEntityRepository.findAll().stream().map(productTypeMapper::toDomain).collect(Collectors.toList());
    }
}
