package com.tul.ecommerce.infraestructure.outbound.repository;

import com.tul.ecommerce.infraestructure.outbound.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CartEntityRepository extends JpaRepository<Cart, String> {
    Optional<Cart> findById(String id);
}
