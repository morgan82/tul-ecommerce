package com.tul.ecommerce.infraestructure.outbound.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
@Setter
@Getter
public class Product {
    @Id
    @Column(unique = true, nullable = false)
    private String sku;
    @Column(nullable = false)
    private String name;
    private String detail;
    @Column(nullable = false)
    private Double price;
    @ManyToOne
    @JoinColumn(name = "product_type_id", nullable = false)
    private ProductType type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getSku().equals(product.getSku());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSku());
    }
}
