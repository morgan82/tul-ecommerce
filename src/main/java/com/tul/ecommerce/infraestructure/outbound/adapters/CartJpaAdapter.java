package com.tul.ecommerce.infraestructure.outbound.adapters;

import com.tul.ecommerce.application.configuration.error.NotFoundException;
import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.domain.model.ItemDomain;
import com.tul.ecommerce.domain.ports.spi.CartPersistencePort;
import com.tul.ecommerce.infraestructure.outbound.entity.Cart;
import com.tul.ecommerce.infraestructure.outbound.entity.Item;
import com.tul.ecommerce.infraestructure.outbound.entity.Product;
import com.tul.ecommerce.infraestructure.outbound.mappers.CartMapper;
import com.tul.ecommerce.infraestructure.outbound.mappers.ItemMapper;
import com.tul.ecommerce.infraestructure.outbound.repository.CartEntityRepository;
import com.tul.ecommerce.infraestructure.outbound.repository.ItemEntityRepository;
import com.tul.ecommerce.infraestructure.outbound.repository.ProductEntityRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Service
public class CartJpaAdapter implements CartPersistencePort {
    private final CartEntityRepository cartEntityRepository;
    private final ItemEntityRepository itemEntityRepository;
    private final ProductEntityRepository productEntityRepository;
    private final CartMapper cartMapper;
    private final ItemMapper itemMapper;
    private final static Predicate<Item> IS_NEW_ITEM = i -> Objects.isNull(i.getId());

    public CartJpaAdapter(CartEntityRepository cartEntityRepository, ItemEntityRepository itemEntityRepository, ProductEntityRepository productEntityRepository, CartMapper cartMapper, ItemMapper itemMapper) {
        this.cartEntityRepository = cartEntityRepository;
        this.itemEntityRepository = itemEntityRepository;
        this.productEntityRepository = productEntityRepository;
        this.cartMapper = cartMapper;
        this.itemMapper = itemMapper;
    }

    @Override
    public CartDomain findCartById(String cartId) {
        return this.cartMapper.toDomain(getCartEntity(cartId));
    }

    @Override
    public CartDomain create(CartDomain cart) {
        Cart cartEntity = this.cartMapper.toEntity(cart);

        return this.cartMapper.toDomain(this.cartEntityRepository.save(cartEntity));
    }

    @Override
    public CartDomain update(CartDomain cart) {
        Cart cartEntity = getCartEntity(cart.getCartId());
        cartEntity.setCartFlow(cart.getCartFlow());

        return this.cartMapper.toDomain(this.cartEntityRepository.save(cartEntity));
    }

    @Override
    public Set<ItemDomain> listItems(String cartId) {
        return this.cartMapper.toDomain(getCartEntity(cartId)).getItemDomains();
    }

    @Override
    public CartDomain addItem(String cartId, ItemDomain itemDomain) {
        Cart cartEntity = getCartEntity(cartId);
        Product productEntity = getProductEntity(itemDomain.getProductDomain().getSku());
        Item itemEntity = new Item(cartEntity, productEntity, itemDomain.getAmount());
        cartEntity.getItems().add(itemEntity);//no cambiar el orden, valida duplicados

        Cart save = this.cartEntityRepository.save(cartEntity);
        return this.cartMapper.toDomain(save);
    }

    @Override
    public CartDomain removeItem(String cartId, String itemId) {
        Cart cartEntity = getCartEntity(cartId);
        Set<Item> items = cartEntity.getItems();
        Optional<Item> itemToRemove = items.stream().filter(item -> item.getId().equals(itemId)).findFirst();
        if (itemToRemove.isPresent() && items.remove(itemToRemove.get())) {
            this.cartEntityRepository.save(cartEntity);
        }

        return this.cartMapper.toDomain(cartEntity);
    }

    @Override
    public CartDomain updateItem(String cartId, String itemId, ItemDomain itemDomain) {
        Cart cartEntity = getCartEntity(cartId);
        Set<Item> items = cartEntity.getItems();
        Optional<Item> itemToUpdate = items.stream().filter(item -> item.getId().equals(itemId)).findFirst();
        itemToUpdate.ifPresent(item -> {
            if (!item.getProduct().getSku().equals(itemDomain.getProductDomain().getSku())) {
                item.setProduct(getProductEntity(itemDomain.getProductDomain().getSku()));
            }
            item.setAmount(itemDomain.getAmount());
            this.itemEntityRepository.save(item);
            this.cartEntityRepository.save(cartEntity);
        });

        return this.cartMapper.toDomain(cartEntity);
    }

    //private methods
    private Cart getCartEntity(String cartId) {
        return this.cartEntityRepository.findById(cartId)
                .orElseThrow(() -> new NotFoundException("Carrito de compras no encontrado"));
    }

    private Product getProductEntity(String sku) {
        return this.productEntityRepository.findBySku(sku)
                .orElseThrow(() -> new NotFoundException("Producto no encontrado"));
    }
}
