package com.tul.ecommerce.infraestructure.outbound.adapters;

import com.tul.ecommerce.application.configuration.error.BadRequestException;
import com.tul.ecommerce.application.configuration.error.NotFoundException;
import com.tul.ecommerce.domain.model.ProductDomain;
import com.tul.ecommerce.domain.ports.spi.ProductPersistencePort;
import com.tul.ecommerce.infraestructure.outbound.entity.Product;
import com.tul.ecommerce.infraestructure.outbound.entity.ProductType;
import com.tul.ecommerce.infraestructure.outbound.mappers.ProductMapper;
import com.tul.ecommerce.infraestructure.outbound.repository.ProductEntityRepository;
import com.tul.ecommerce.infraestructure.outbound.repository.ProductTypeEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductJpaAdapter implements ProductPersistencePort {
    private final ProductEntityRepository productEntityRepository;
    private final ProductTypeEntityRepository productTypeEntityRepository;
    private final ProductMapper productMapper;

    public ProductJpaAdapter(ProductEntityRepository productEntityRepository, ProductTypeEntityRepository productTypeEntityRepository, ProductMapper productMapper) {
        this.productEntityRepository = productEntityRepository;
        this.productTypeEntityRepository = productTypeEntityRepository;
        this.productMapper = productMapper;
    }

    @Override
    public Optional<ProductDomain> findProductBySku(String sku) {
        try {
            return Optional.of(this.productMapper.toDomain(getProductBySku(sku)));
        } catch (NotFoundException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<ProductDomain> list() {
        return this.productEntityRepository.findAll().stream().map(productMapper::toDomain).collect(Collectors.toList());
    }

    @Override
    public ProductDomain save(ProductDomain p) {
        ProductType productType = getProductTypeById(p.getType().getProductTypeIdentifier());
        Product product = productMapper.toEntity(p);
        product.setType(productType);

        return this.productMapper.toDomain(this.productEntityRepository.save(product));
    }

    @Override
    public ProductDomain update(String sku, ProductDomain p) {
        Product product = getProductBySku(sku);
        ProductType productType = getProductTypeById(p.getType().getProductTypeIdentifier());
        product.setPrice(p.getPrice());
        product.setDetail(p.getDetail());
        product.setName(p.getName());
        product.setType(productType);
        product.setSku(p.getSku());
        return this.productMapper.toDomain(productEntityRepository.save(product));
    }

    @Override
    public void delete(String sku) {
        this.productEntityRepository.delete(getProductBySku(sku));
    }

    //private methods
    private Product getProductBySku(String sku) {
        return this.productEntityRepository.findBySku(sku).orElseThrow(() -> new NotFoundException("Producto no encontrado"));
    }

    private ProductType getProductTypeById(String id) {
        return this.productTypeEntityRepository.findById(id).orElseThrow(() -> new BadRequestException("Tipo de Producto no encontrado"));
    }
}
