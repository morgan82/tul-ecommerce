package com.tul.ecommerce.infraestructure.outbound.mappers;

import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.infraestructure.outbound.entity.Cart;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = ItemMapper.class)
public interface CartMapper {
    @Mappings({
            @Mapping(source = "cartId", target = "id"),
            @Mapping(source = "itemDomains", target = "items"),
    })
    Cart toEntity(CartDomain cartDomain);

    @Mappings({
            @Mapping(source = "id", target = "cartId"),
            @Mapping(source = "items", target = "itemDomains"),
    })
    CartDomain toDomain(Cart cart);

}
