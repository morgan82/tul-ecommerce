package com.tul.ecommerce.infraestructure.outbound.repository;

import com.tul.ecommerce.infraestructure.outbound.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeEntityRepository extends JpaRepository<ProductType, String> {
}
