package com.tul.ecommerce.infraestructure.outbound.repository;

import com.tul.ecommerce.infraestructure.outbound.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductEntityRepository extends JpaRepository<Product, String> {
    Optional<Product> findBySku(String sku);
}
