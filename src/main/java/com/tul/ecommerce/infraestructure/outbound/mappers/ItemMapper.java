package com.tul.ecommerce.infraestructure.outbound.mappers;

import com.tul.ecommerce.domain.model.ItemDomain;
import com.tul.ecommerce.infraestructure.outbound.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ItemMapper {
    @Mappings({
            @Mapping(source = "productDomain", target = "product"),
    })
    Item toEntity(ItemDomain itemDomain);

    @Mappings({
            @Mapping(source = "product", target = "productDomain"),
            @Mapping(source = "id", target = "itemId"),
    })
    ItemDomain toDomain(Item item);
}
