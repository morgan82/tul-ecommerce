package com.tul.ecommerce.infraestructure.outbound.entity;

import com.tul.ecommerce.domain.model.ProductTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class ProductType {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)")
    private String id;
    @Column(nullable = false, length = 50)
    private String detail;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    private ProductTypeEnum type;
    private Double discount;
}
