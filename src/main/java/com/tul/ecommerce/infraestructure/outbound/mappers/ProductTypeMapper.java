package com.tul.ecommerce.infraestructure.outbound.mappers;

import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.infraestructure.outbound.entity.ProductType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ProductTypeMapper {
    @Mappings({
            @Mapping(source = "id", target = "productTypeIdentifier"),
    })
    ProductTypeDomain toDomain(ProductType product);
}
