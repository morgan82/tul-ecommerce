package com.tul.ecommerce.infraestructure.inbound.api;

import com.tul.ecommerce.application.configuration.swagger.SwaggerConfig;
import com.tul.ecommerce.domain.ports.api.CartService;
import com.tul.ecommerce.infraestructure.inbound.mapper.CartApiMapper;
import com.tul.ecommerce.infraestructure.inbound.mapper.CheckoutApiMapper;
import com.tul.ecommerce.infraestructure.inbound.mapper.ItemApiMapper;
import com.tul.ecommerce.infraestructure.inbound.model.in.ItemInDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.CartCreatedOutDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.CartOutDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@Api(tags = {SwaggerConfig.MICROSERVICES_CART_TAG})
@RequestMapping("/cart")
public class CartController {
    private final CartService cartService;
    private final CartApiMapper cartApiMapper;
    private final CheckoutApiMapper checkoutApiMapper;
    private final ItemApiMapper itemApiMapper;

    public CartController(CartService cartService, CartApiMapper cartApiMapper, CheckoutApiMapper checkoutApiMapper, ItemApiMapper itemApiMapper) {
        this.cartService = cartService;
        this.cartApiMapper = cartApiMapper;
        this.checkoutApiMapper = checkoutApiMapper;
        this.itemApiMapper = itemApiMapper;
    }

    @ApiOperation(value = "Crea un carrito de compras sin productos")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "la operación se ejecutó correctamente")})
    @PostMapping
    public ResponseEntity<CartCreatedOutDTO> create() {
        return new ResponseEntity<>(this.cartApiMapper.toDTOCreated(this.cartService.createCart()), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Agrega un item al carrito")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @PostMapping("/{cartId}/item")
    public CartOutDTO addItem(@Valid @RequestBody ItemInDTO itemInDTO, @NotBlank @PathVariable String cartId) {
        return this.cartApiMapper.toDTOFull(this.cartService.addItem(cartId, this.itemApiMapper.toDomain(itemInDTO)));
    }

    @ApiOperation(value = "Elimina un item del carrito")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @DeleteMapping("/{cartId}/item/{itemId}")
    public CartOutDTO removeItem(@NotBlank @PathVariable String cartId, @NotBlank @PathVariable String itemId) {
        return this.cartApiMapper.toDTOFull(this.cartService.removeItem(cartId, itemId));
    }

    @ApiOperation(value = "Obtiene del detalle de un carrito ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @GetMapping("/{cartId}")
    public CartOutDTO getCart(@NotBlank @PathVariable String cartId) {
        return this.cartApiMapper.toDTOFull(this.cartService.getCart(cartId));
    }

    @ApiOperation(value = "Modifica el item de un carrito ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @PutMapping("/{cartId}/item/{itemId}")
    public CartOutDTO getCart(@NotBlank @PathVariable String cartId,
                              @NotBlank @PathVariable String itemId,
                              @Valid @RequestBody ItemInDTO itemInDTO) {
        return this.cartApiMapper.toDTOFull(this.cartService.updateItem(cartId, itemId, this.itemApiMapper.toDomain(itemInDTO)));
    }

    @ApiOperation(value = "Cierra el carrito y calcula total")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @PutMapping("/{cartId}/checkout")
    public CartOutDTO checkout(@NotBlank @PathVariable String cartId) {
        return this.checkoutApiMapper.toCheckoutDTO(this.cartService.checkout(cartId));
    }

}

