package com.tul.ecommerce.infraestructure.inbound.api;

import com.tul.ecommerce.application.configuration.swagger.SwaggerConfig;
import com.tul.ecommerce.domain.ports.api.ProductService;
import com.tul.ecommerce.infraestructure.inbound.mapper.ProductApiMapper;
import com.tul.ecommerce.infraestructure.inbound.model.in.ProductInDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.ProductOutDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = {SwaggerConfig.MICROSERVICES_PRODUCT_TAG})
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    private final ProductApiMapper productApiMapper;

    public ProductController(ProductService productService, ProductApiMapper productApiMapper) {
        this.productService = productService;
        this.productApiMapper = productApiMapper;
    }

    @ApiOperation(value = "Crear un producto")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "la operación se ejecutó correctamente"),
            @ApiResponse(code = 400, message = "Algun parametro no cumple con el formato o es requerido y no esta presente")})
    @PostMapping
    public ProductOutDTO createProduct(@Valid @RequestBody ProductInDTO productInDTO) {
        return this.productApiMapper.toDTO(
                this.productService.createProduct(productApiMapper.toDomain(productInDTO)));
    }

    @ApiOperation(value = "Actualiza un producto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente"),
            @ApiResponse(code = 400, message = "Algun parametro no cumple con el formato o es requerido y no esta presente")})
    @PutMapping("/{sku}")
    public ProductOutDTO updateProduct(@Valid @RequestBody ProductInDTO productInDTO, @NotBlank @PathVariable String sku) {
        return this.productApiMapper.toDTO(
                this.productService.updateProduct(sku, productApiMapper.toDomain(productInDTO)));
    }

    @ApiOperation(value = "Lista todos los productos")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente"),
            @ApiResponse(code = 400, message = "Algun parametro no cumple con el formato o es requerido y no esta presente")})
    @GetMapping
    public List<ProductOutDTO> listProducts() {
        return this.productService.listProducts().stream().map(productApiMapper::toDTO).collect(Collectors.toList());
    }

    @ApiOperation(value = "Elimina un producto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente"),
            @ApiResponse(code = 400, message = "Algun parametro no cumple con el formato o es requerido y no esta presente")})
    @DeleteMapping("/{sku}")
    public void deleteProduct(@NotBlank @PathVariable String sku) {
        productService.deleteProduct(sku);
    }


}
