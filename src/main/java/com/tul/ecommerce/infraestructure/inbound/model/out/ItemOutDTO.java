package com.tul.ecommerce.infraestructure.inbound.model.out;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.tul.ecommerce.infraestructure.inbound.model.in.ProductInDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Setter
@Getter
@ToString
public class ItemOutDTO {
    private String itemId;
    private ProductInDTO product;
    private Long amount;
    private Double totalDiscount;
    private Double discount;
}
