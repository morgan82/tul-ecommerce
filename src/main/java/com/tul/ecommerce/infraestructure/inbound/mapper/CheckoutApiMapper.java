package com.tul.ecommerce.infraestructure.inbound.mapper;

import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.infraestructure.inbound.model.out.CartOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = CheckoutItemApiMapper.class)
public interface CheckoutApiMapper {
    @Mappings({
            @Mapping(source = "itemDomains", target = "items"),
    })
    CartOutDTO toCheckoutDTO(CartDomain cartDomain);
}
