package com.tul.ecommerce.infraestructure.inbound.api;

import com.tul.ecommerce.application.configuration.swagger.SwaggerConfig;
import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.domain.ports.api.ProductTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(tags = {SwaggerConfig.MICROSERVICES_PRODUCT_TYPE_TAG})
@RequestMapping("/product_type")
public class ProductTypeController {
    private final ProductTypeService productTypeService;

    public ProductTypeController(ProductTypeService productTypeService) {
        this.productTypeService = productTypeService;
    }

    @ApiOperation(value = "Lista todos los tipos de productos")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "la operación se ejecutó correctamente")})
    @GetMapping
    public List<ProductTypeDomain> listProductProductTypes() {
        return productTypeService.listProductTypes();
    }
}

