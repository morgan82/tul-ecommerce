package com.tul.ecommerce.infraestructure.inbound.model.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Setter
@Getter
public class ItemInDTO {

    @ApiModelProperty(notes = "Stock-keeping unit codigo unico de identificacion del producto", example = "f9405430-7966-11ec-90d6-0242ac120003")
    @NotNull
    private String productSku;
    @ApiModelProperty(notes = "cantidad del producto, mayor a 0", example = "1")
    @Positive
    private Long amount;
}
