package com.tul.ecommerce.infraestructure.inbound.model.in;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Setter
@Getter
public class ProductInDTO {
    @ApiModelProperty(notes = "Stock-keeping unit codigo unico de identificacion del producto", example = "f9405430-7966-11ec-90d6-0242ac120003")
    @NotBlank
    private String sku;
    @ApiModelProperty(notes = "nombre del producto", example = "cemento")
    @NotBlank
    private String name;
    @ApiModelProperty(notes = "detalle del producto", example = "bolsa de cemento LOMA NEGRA x 50 kg.")
    private String detail;
    @ApiModelProperty(notes = "precion del producto", example = "150")
    @Positive
    private Double price;
    @NotBlank
    @ApiModelProperty(notes = "identificador unico del tipo de producto", example = "listar los tipos y seleccionar un identificador")
    private String productTypeId;
}
