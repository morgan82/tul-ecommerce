package com.tul.ecommerce.infraestructure.inbound.mapper;

import com.tul.ecommerce.domain.model.ProductDomain;
import com.tul.ecommerce.domain.model.ProductTypeDomain;
import com.tul.ecommerce.infraestructure.inbound.model.in.ProductInDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.ProductOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ProductApiMapper {
    @Mappings({
            @Mapping(source = "productTypeId", target = "type", qualifiedByName = "productTypeDTOToDomain"),
    })
    ProductDomain toDomain(ProductInDTO productInDTOct);

    @Mappings({
            @Mapping(source = "type", target = "category"),
    })
    ProductOutDTO toDTO(ProductDomain productDomaint);

    @Named("productTypeDTOToDomain")
    default ProductTypeDomain productTypeDTOToDomain(String productTypeId) {
        ProductTypeDomain type = new ProductTypeDomain();
        type.setProductTypeIdentifier(productTypeId);
        return type;
    }
}
