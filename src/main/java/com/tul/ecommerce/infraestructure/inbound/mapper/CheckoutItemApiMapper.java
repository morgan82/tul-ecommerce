package com.tul.ecommerce.infraestructure.inbound.mapper;

import com.tul.ecommerce.domain.model.ItemDomain;
import com.tul.ecommerce.infraestructure.inbound.model.out.ItemOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CheckoutItemApiMapper {
    @Mappings({
            @Mapping(source = "productDomain", target = "product"),
            @Mapping(source = "totalItem", target = "totalDiscount"),
            @Mapping(source = "productDomain.type.discount", target = "discount"),
    })
    ItemOutDTO toCheckoutDTO(ItemDomain itemDomain);
}
