package com.tul.ecommerce.infraestructure.inbound.mapper;

import com.tul.ecommerce.domain.model.CartDomain;
import com.tul.ecommerce.infraestructure.inbound.model.out.CartCreatedOutDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.CartOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = ItemApiMapper.class)
public interface CartApiMapper {
    @Mappings({
            @Mapping(source = "itemDomains", target = "items"),
    })
    CartOutDTO toDTOFull(CartDomain cartDomain);

    CartCreatedOutDTO toDTOCreated(CartDomain cartDomain);
}
