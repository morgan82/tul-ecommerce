package com.tul.ecommerce.infraestructure.inbound.mapper;

import com.tul.ecommerce.domain.model.ItemDomain;
import com.tul.ecommerce.infraestructure.inbound.model.in.ItemInDTO;
import com.tul.ecommerce.infraestructure.inbound.model.out.ItemOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ItemApiMapper {
    @Mappings({
            @Mapping(source = "productDomain", target = "product"),
    })
    ItemOutDTO toDTO(ItemDomain itemDomain);

    @Mappings({
            @Mapping(source = "productSku", target = "productDomain.sku"),
    })
    ItemDomain toDomain(ItemInDTO itemInDTO);

}
