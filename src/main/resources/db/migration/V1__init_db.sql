create table dbo.cart (
            id varchar(110) not null,
            cart_flow varchar(15) not null,
            primary key (id));

create table dbo.item (
            id varchar(110) not null,
            amount bigint not null,
            cart_id varchar(110) not null,
            product_id varchar(110) not null,
            primary key (id));

create table dbo.product (
            sku varchar(110) not null,
            detail varchar(5000),
            name varchar(150) not null,
            price double precision not null,
            product_type_id VARCHAR(110) not null
            primary key (sku));

create table dbo.product_type (
            id VARCHAR(110) not null,
            discount double precision,
            type varchar(15) not null,
            detail varchar(50) not null,
            primary key (id));

alter table dbo.product add constraint FKlabq3c2e90ybbxk58rc48byqo foreign key (product_type_id) references dbo.product_type;
alter table dbo.item add constraint FK4g2q77pbbf0faqae5uywbsodk foreign key (cart_id) references dbo.cart;
alter table dbo.item add constraint FKd1g72rrhgq1sf7m4uwfvuhlhe foreign key (product_id) references dbo.product;
