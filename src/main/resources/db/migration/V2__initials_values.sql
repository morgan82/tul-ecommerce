--Product Type
INSERT INTO [dbo].[product_type]([id],[discount],[type],[detail])
     VALUES('16a697d2-7a02-11ec-90d6-0242ac120003',0,'SIMPLE','sin descuento'),
			('474d1776-7a02-11ec-90d6-0242ac120003',0.5,'DISCOUNT','descuento del 50 %');
--Product
INSERT INTO [dbo].[product]([sku],[detail],[name],[price],[product_type_id])
     VALUES ('72b9363c-9d12-44eb-9cd0-be8a4bcd3922','Fresa para minitorno eje 3mm','Fresas Mini Torno Multiproposito Madera Minitorno X6 Pcs',10,'16a697d2-7a02-11ec-90d6-0242ac120003'),
		    ('e5476bd8-7a44-11ec-90d6-0242ac120003','Losetas Atérmicas 50 X 50 Cm','Fabricante FB CONSTRUCCIONES, Nombre del diseño FB',100,'474d1776-7a02-11ec-90d6-0242ac120003'),
		    ('299256c2-7a45-11ec-90d6-0242ac120003','Relleno Contrapiso Telgopor, Construcción, G Y G Fiacas','Marca G Y G FIACAS, Modelo RELLENO',1000,'474d1776-7a02-11ec-90d6-0242ac120003'),
		    ('3bf97c00-7a45-11ec-90d6-0242ac120003','Perfil De Aluminio Angulo 25x25mm Natural Largo X 6 Metros','Marca PERFIMET, Modelo ANGULO NATURAL DE 25x25x1.1 mm, Forma del perfil ANGULO, Largo x Ancho 6 m x 25 mm, Espesor 1.1 mm',10000,'16a697d2-7a02-11ec-90d6-0242ac120003')