# tul-ecommerce



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:ce42feaadbf4c35aff70f7b58a1c612b?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:ce42feaadbf4c35aff70f7b58a1c612b?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:ce42feaadbf4c35aff70f7b58a1c612b?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/morgan82/tul-ecommerce.git
git branch -M main
git push -uf origin main
```

## Empezando
### Clonar el repositorio

git clone https://gitlab.com/morgan82/tul-ecommerce.git .

git checkout develop

### Compilación y ejecución de la Api

gradlew clean build

docker-compose up

o en caso de que haya colicion con imagenes o contenedores

docker-compose up --build

### URL de la Api

http://localhost:8080/swagger-ui.html